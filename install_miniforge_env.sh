#!/usr/bin/env bash

arguments="1:INSTALL_DIR 2:CONDA_ENVIRONMENT_URL"
if [[ $# -ne 2 ]]; then
    echo "NAME:"
    echo -e "\t${0}"
    echo
    echo "SYNOPSIS:"
    echo -e "\tbash ${0} ${arguments}"
    echo
    echo "DESCRIPTION:"
    echo -e "\tInstall Miniforge and create an environment"
    echo
    echo -e "\tINSTALL_DIR must be empty."
    echo -e "\tCONDA_ENVIRONMENT_URL is the URL of a Conda environment in YAML."
    echo -e "\tThe environment file aims at accelerating the installation."
    echo 
    echo "EXAMPLES:"
    echo -e "\tbash ${0} miniconda https://gitlab.com/api/v4/projects/uranie-cea%2Fpublication/packages/generic/experimental/4.9.0/uranie_env.yml"
    echo 
    echo "REPORTING BUGS:"
    echo -e "\tCEA Uranie: support-uranie@cea.fr"
    exit -1
else
    echo "arguments: [${arguments}]"
fi

i_arg=0
for arg in "$@"; do
    i_arg=$((${i_arg}+1))
    echo "${0} :: ${i_arg}: ${arg}"
done

set -ex

MINICONDA_URL=https://github.com/conda-forge/miniforge/releases/latest/download
MINICONDA_INSTALLER=Miniforge3-Linux-x86_64.sh
CONDA_ENVIRONMENT_NAME=$(basename ${2} ".yml")

wget -c ${MINICONDA_URL}/${MINICONDA_INSTALLER}
sha256sum ${MINICONDA_INSTALLER}
bash ${MINICONDA_INSTALLER} -b -p ${1}
rm ${MINICONDA_INSTALLER}

set +e
source ${1}/bin/activate
set -e

conda update -y -n base -c defaults conda
conda env create --yes -f ${2}

set +x

echo
printf '=%.0s' {1..60}
echo
echo "In a new bash session, please type:"
echo "source $(readlink -f ${1})/bin/activate ${CONDA_ENVIRONMENT_NAME}"
printf '=%.0s' {1..60}
echo
