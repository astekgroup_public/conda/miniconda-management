Miniconda Management
====================

The repository contains files to manage Miniconda installations with GitLab for
CEA Uranie project.

.. only:: html

   .. note:: A PDF version of this documentation may be available, see:
      https://gitlab.com/uranie-cea/conda/miniconda-management/-/packages.

.. only:: latex

   .. note:: A HTML version of this documentation may be available, see:
      https://uranie-cea.gitlab.io/conda/miniconda-management/.

.. git_commit_detail::
   :branch:
   :commit:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installations
   environments
   building
   cleaning
   indices_and_tables
