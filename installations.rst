Installations
=============

Templates
---------

Hidden installation jobs are described:

* ``.install_miniconda``: install Miniconda on Linux
* ``.install_packages``: install packages inside an environment
* ``.install_miniconda_standalone``: install Miniconda and create an environment
  from scratch.

.. literalinclude:: installations.yml
    :language: yaml
    :caption:

See https://conda.io/projects/conda/en/latest/user-guide/install/index.html.

Script
------

Current standalone installation script used in ``.install_miniconda_standalone``
is shown below:

.. literalinclude:: install_miniforge_env.sh
    :language: bash
    :caption:

See
https://docs.anaconda.com/anacondaorg/user-guide/tasks/work-with-environments/.

.. note::

    * ``MINICONDA_INSTALLER`` is hard coded.
    * ``CONDA_ENVIRONMENT`` filename and ``name`` must be coherent.

Examples
--------

Templates are tested in the following concrete jobs:

.. literalinclude:: tests/installations.yml
    :language: yaml
    :caption:

In a child pipeline of
https://gitlab.com/uranie-cea/conda/miniconda-management/-/pipelines, you
may find the "Install" stage with the previous jobs.
