Cleaning
========

Templates
---------

Hidden cleaning jobs are described:

* ``.remove_environment``: remove a Conda environment
* ``.remove_miniconda``: remove Miniconda installation
* ``.remove_installer``: remove Miniconda installer.

.. literalinclude:: cleaning.yml
    :language: yaml
    :caption:

See
https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#removing-an-environment.

Examples
--------

Templates are tested in the following concrete jobs:

.. literalinclude:: tests/cleaning.yml
    :language: yaml
    :caption:

In a child pipeline of
https://gitlab.com/uranie-cea/conda/miniconda-management/-/pipelines, you
may find the "Clean" stage with the previous jobs.
