Environments
============

.. _miniconda_environments_templates:

Templates
---------

Hidden environment jobs are described:

* Conda environment creation from differents sources:

  * ``.create_environment``: packages
  * ``.create_environment_from_file``: file with packages list
  * ``.create_environment_from_url``: URL to environment file.

* ``.activate_environment``: activate an environment
* ``.upload_environment``: upload an environment file to URL.

.. note:: To communicate with Anaconda.org, you may need to:

    * install ``anaconda-client`` package
    * login with ``anaconda``.

    See
    https://docs.anaconda.com/anacondaorg/user-guide/tasks/work-with-packages/.

.. literalinclude:: environments.yml
    :language: yaml
    :caption:

See
https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html.

Examples
--------

Some templates are tested in the following concrete jobs:

.. literalinclude:: tests/environments.yml
    :language: yaml
    :caption:

In a child pipeline of
https://gitlab.com/uranie-cea/conda/miniconda-management/-/pipelines, you
may find the "Environment" stage with the previous jobs.
