Building
========

Templates
---------

``.build_package`` hidden job aims at building a Conda package of
``CONDA_RECIPE`` in ``CONDA_ENVIRONMENT``:

.. literalinclude:: building.yml
    :language: yaml
    :caption:

See https://docs.conda.io/projects/conda-build/en/latest/user-guide/index.html.

.. note::

    * To communicate with Anaconda.org, see
      :ref:`miniconda_environments_templates`.
    * ``CONDA_BUILD_COMMAND`` can be set to use ``build`` or a different
      command.
